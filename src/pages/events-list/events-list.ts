import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { EventDetailsPage, AddEventPage } from '../pages';
import { IReport } from '../../interfaces/report';
import { JavaApi } from '../../shared/java-api';
import { ThrowStmt } from '@angular/compiler';
import { IInterface } from '../../interfaces/assessment';

/**
 * Generated class for the EventsListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-events-list',
  templateUrl: 'events-list.html',
})
export class EventsListPage {

  events: string[] = ["1", "2", "3"];
  reports: IReport[] = [];
  tmpReport: any = {};
  ratings: any[] = [];

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public javaApi: JavaApi) { }

  ionViewDidLoad() {     
    console.log('ionViewDidLoad EventsListPage');
  }

  ionViewWillEnter(){
    console.log(this.javaApi.getCurrentUSer());
    this.reports = [];
    let tmpReports = [];
    let like : number = 0;
    let dislike: number = 0;
    this.javaApi.getReports()
      .flatMap(reports => reports)
      .subscribe((report : IReport)=>{
        this.reports.push(report);
        this.javaApi.getAssessmentsByReportId(report.id)
        .subscribe((data : IInterface)=>{
          like = 0;
          dislike = 0;
          for(let i in data){
            if(data[i].assessment == "LIKE"){
              like++;
            }else if(data[i].assessment == "DISLIKE"){
              dislike++;
            }
          }
          for(let j in data){
            for(let z in this.reports){
              if(data[j].reportId == this.reports[z].id){
                this.reports[z].likes = like;
                this.reports[z].dislikes = dislike;
              }
            }
          }
        });
      }) 
  }

  showDetails(report: IReport){    
    console.log("tmpRatings " + this.tmpReport);
    this.navCtrl.push(EventDetailsPage, {report: report});
  }

  LogOut(){    
    this.navCtrl.pop();
  }

  
  addReport(){
    this.navCtrl.push(AddEventPage);
  }

}
