export * from './home/home';
export * from './login/login';
export * from './register/register';
export * from './events-list/events-list';
export * from './event-details/event-details';
export * from './events-list/events-list';
export * from './add-event/add-event';