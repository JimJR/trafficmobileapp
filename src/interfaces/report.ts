import { DateTime } from "ionic-angular";

export interface IReport{
    id: number,
    issue: string,
    issueDate: Date,
    lat: number,
    lng: number,
    userId: number,
    likes: number,
    dislikes: number
}