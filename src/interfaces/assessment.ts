export interface IInterface{
    id: number,
    assessment: string,
    reportId: number,
    userId: number
}