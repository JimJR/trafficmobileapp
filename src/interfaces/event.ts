export interface IEvent{
    id: number,
    userID: number,
    issue: string,
    issueDate: Date,
    type: EventType
}

enum EventType {
    
}